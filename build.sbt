name := "TriMet Alerts for Glass"

organization := "com.cmdev"
 
version := "1.0"
 
scalaVersion := "2.10.2"
 
scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

seq(webSettings: _*)

// A hack to set context path for jetty to "/something"
env in Compile := Some(file(".") / "sbt-jetty-env.xml" asFile)
 
resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
  "com.google.apis" % "google-api-services-mirror" % "v1-rev26-1.17.0-rc",
  "com.google.http-client" % "google-http-client-jackson2" % "1.17.0-rc",
  // servlet
  "javax.servlet" % "servlet-api" % "2.5" % "provided",
  // Jetty (for sbt web plugin)
  "org.eclipse.jetty" % "jetty-server" % "7.2.2.v20101205" % "container",
  "org.eclipse.jetty" % "jetty-webapp" % "7.2.2.v20101205" % "container",
  "org.eclipse.jetty" % "jetty-jsp-2.1" % "7.2.2.v20101205" % "container",
  "org.eclipse.jetty" % "jetty-plus" % "7.2.2.v20101205" % "container",
  "org.mortbay.jetty" % "jsp-2.1-glassfish" % "2.1.v20100127" % "container",
  // various
  "org.apache.commons" % "commons-lang3" % "3.1",
  "commons-logging" % "commons-logging" % "1.1.2",
  "commons-codec" % "commons-codec" % "1.7",
  "com.google.guava" % "guava" % "14.0.1",
  "org.apache.httpcomponents" % "fluent-hc" % "4.2.5",
  // json
  "com.fasterxml.jackson.core" % "jackson-core" % "2.3.1",
  "com.fasterxml.jackson.core" % "jackson-annotations" % "2.2.0",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.3.1",
  // date, time, intervals, durations
  "joda-time" % "joda-time" % "2.3",
  "org.joda" % "joda-convert" % "1.5",
  // akka
  "com.typesafe.akka" %% "akka-actor" % "2.2.3",
  // testing
  "junit" % "junit" % "4.10" % "test",
  "com.novocode" % "junit-interface" % "0.10" % "test",
  "org.scalatest" % "scalatest_2.10" % "2.0" % "test"
)
