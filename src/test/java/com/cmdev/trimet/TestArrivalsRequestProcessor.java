package com.cmdev.trimet;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestArrivalsRequestProcessor {

    @Test
    public void testRequest() {
	ArrivalsRequestProcessor arp = new ArrivalsRequestProcessor();
	arp.request("7602");
	assertNotNull("No locations found", arp.getLocations());
	assertEquals("One stopid, one location", 1, arp.getLocations().size());
	assertNotNull("No arrivals found", arp.getArrivals());
	Arrival a = arp.getArrival(0);
	assertNotNull("Arrival 0 not found", a);
	assertTrue("Route "+a.getRoute()+" should have departed", a.isDeparted());
    }
}
