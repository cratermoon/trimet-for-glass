package com.cmdev.trimet.json;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.LogManager;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.junit.Assert.*;
import org.junit.Test;

import com.cmdev.trimet.Arrival;

public class TestArrival {

  private static final String TEST_ARRIVAL_JSON = "{\"resultSet\":{\"location\":[{\"desc\":\"SW Capitol & 33rd\",\"locid\":971,\"dir\":\"Eastbound\",\"lng\":-122.710173128716,\"lat\":45.4685494547266}],\"arrival\":[{\"detour\":false,\"status\":\"estimated\",\"locid\":971,\"block\":4403,\"scheduled\":\"2014-01-02T10:53:41.000-0800\",\"shortSign\":\"44 To St Johns\",\"dir\":1,\"estimated\":\"2014-01-02T10:53:27.000-0800\",\"route\":44,\"departed\":true,\"blockPosition\":{\"at\":\"2014-01-02T10:48:37.000-0800\",\"feet\":7569,\"lng\":-122.7221974,\"trip\":[{\"progress\":6254,\"desc\":\"Lombard & Pier Park\",\"pattern\":2,\"dir\":1,\"route\":44,\"tripNum\":\"4219919\",\"destDist\":13824}],\"lat\":45.4519641,\"heading\":26},\"fullSign\":\"44  Capitol Hwy/Mocks Crest to St Johns via City Center\",\"piece\":\"1\"},{\"detour\":false,\"status\":\"estimated\",\"locid\":971,\"block\":4404,\"scheduled\":\"2014-01-02T11:23:41.000-0800\",\"shortSign\":\"44 To Portland\",\"dir\":1,\"estimated\":\"2014-01-02T11:23:41.000-0800\",\"route\":44,\"departed\":false,\"blockPosition\":{\"at\":\"2014-01-02T10:47:49.000-0800\",\"feet\":21011,\"lng\":-122.7199917,\"trip\":[{\"progress\":35837,\"desc\":\"PCC-Sylvania\",\"pattern\":4,\"dir\":0,\"route\":44,\"tripNum\":\"4219763\",\"destDist\":43025},{\"progress\":0,\"desc\":\"North Terminal Westside\",\"pattern\":1,\"dir\":1,\"route\":44,\"tripNum\":\"4219920\",\"destDist\":13824}],\"lat\":45.4553112,\"heading\":207},\"fullSign\":\"44  Capitol Hwy to Portland\",\"piece\":\"1\"}],\"queryTime\":\"2014-01-02T10:48:48.342-0800\"}}";

  @Test
  public void testCreate() throws java.io.IOException {
      LogManager.getLogManager().readConfiguration(Thread.currentThread().getContextClassLoader().getResourceAsStream("logging.properties"));
    ObjectMapper mapper = new ObjectMapper();
    TriMetArrivalsResponse arrivals = mapper.readValue(TEST_ARRIVAL_JSON, TriMetArrivalsResponse.class);
    Arrival anArrival = new Arrival(arrivals.resultSet.arrival.get(0));
    assertTrue(anArrival.isDeparted());
    assertEquals("Wrong route number", 44, anArrival.getRoute());
    assertEquals("Incorrect full sign text", "44  Capitol Hwy/Mocks Crest to St Johns via City Center", anArrival.getFullSignText());
    assertFalse("Should not be detoured", anArrival.isDetoured());
  }
}
