package com.cmdev.trimet;

import static org.junit.Assert.*;

import org.junit.Test;

import com.cmdev.trimet.json.TriMetArrivalsResponse;

public class TestGetArrivals {

	@Test
	public void test() {
		Arrival a = new Arrival("971");
		//		System.out.println(a.nextArrivalLong());
		//		System.out.println(a.stopDescription);
		//		System.out.println(a.getRoute());
		//		System.out.println(a.nextArrivalTimeStamp);
		//		System.out.println(a.getStatus());
		//		System.out.println(a.isDeparted());
                Location location = a.getLocation(0);
		//		System.out.println(location.lat);
		//		System.out.println(location.lng);
		//		System.out.println(location.dir);
		//		System.out.println(location.desc);
		assertTrue("expected an arrival time in the future", a.nextArrivalLong() > 0);
	}

}
