package com.cmdev.tma4g.notifications

import org.scalatest._

class EventsSpec extends FlatSpec with Matchers {

  "An Event" should "have an owner, id, timestamp, frequency, and time" in {
     val testEvent = Event("Me", 1234L, 1234, 1234)
     testEvent.owner should equal ("Me")
     testEvent.id should be > 0
     testEvent.id should be < Int.MaxValue
     testEvent.timestamp should be (1234)
     testEvent.recurFrequencySeconds should be (1234)
     testEvent.leadTime should be (1234)
  }

  "An EventStore" should "be empty on creation" in {
    EventStore.isEmpty() should be (true)
  }

  it should "store and retrieve Event lists by owner" in {

    val testEvent = Event("Me", 1234L, 1234, 1234)
    EventStore.put(testEvent)
    val myEvents = EventStore.getAllForOwner("Me")
    myEvents.size should be (1)
    val firstEvent = myEvents.get(testEvent.id)
    firstEvent match  {
      case None => fail("firstEvent was None: "+firstEvent)
      case Some(e: Event) => e.owner should equal (testEvent.owner)
    }
  }
}
