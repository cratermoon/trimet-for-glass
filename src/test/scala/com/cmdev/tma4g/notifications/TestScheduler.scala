 package com.cmdev.tma4g.notifications

import org.scalatest._

class SchedulerSpec extends FlatSpec with Matchers {

	class TestListener(n: Notification) extends ArrivalCallback(n) {
		def callback = {
			println("***** Hiiiii!!!!! *****")
		}
	}
	"An AlertScheduler" should "run when started" in {
		val as = new AlertScheduler()
    	as.start
    	as.scheduleAlert(44, 9)
    	as.scheduleAlert(35, 15)
    
    	val tl = new TestListener(Notification("id", 44, 11))
    	as.scheduleAlert(tl)
	    Thread.sleep(70 * 1000)
    	as.shutdown()
	}

}