package com.cmdev.tma4g.notifications

import scala.collection.Map
import scala.collection.mutable
import scala.util.Random

case class Event(
  owner: String,
  timestamp: Long,
  recurFrequencySeconds: Int,
  leadTime: Int
) {
  val rng = new Random()

  val id = rng.nextInt(Int.MaxValue)
}

class EventStore {}

object EventStore {
  val eventsByOwner = mutable.Map[String, Map[Int, Event]]()

  def put(evt: Event) = {
    val ownerEvents = getEvents(evt.owner)
    eventsByOwner put (evt.owner, ownerEvents + (evt.id -> evt))
  }

  private def getEvents(owner: String): Map[Int, Event] = {
    eventsByOwner getOrElse (owner, Map[Int, Event]())
  }

  def remove(evt: Event) = {
    val ownerEvents = getEvents(evt.owner)
    eventsByOwner put (evt.owner, ownerEvents - evt.id)
  }

  def getAllForOwner(owner: String): Map[Int, Event] = {
    getEvents(owner)
  }

  def isEmpty(): Boolean = {
    eventsByOwner.isEmpty
  }
}

class EventProcessor {
  // every minute, check the list of alerts, fire any that are due
  // when an event fires, get the stopId and route, check with trimet
  // if the next arrival time is less than or equal to the alarm time,
  // send a notification to the user
  // Scheduler.schedule(() => println("Do something"), 1L, 1L, TimeUnit.MINUTES)
}
