package com.cmdev.tma4g.notifications

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration._
import scala.util.Random

import akka.actor._
import akka.event.Logging

import org.joda.time.DateTime

class AlertScheduler {

  implicit val system = akka.actor.ActorSystem("system")

  import system.dispatcher

  val checker = system.actorOf(Props[ArrivalChecker], "arrivalChecker")
  val prng = new Random()

  def scheduleAlert(route: Int, minutes: Int) = {
    val id = prng.alphanumeric.take(16).mkString
    checker ! Notification(id, route, minutes)
  }

  def scheduleAlert(ac: ArrivalCallback) = {
    checker ! ac
  }

  def start() = {
    system.scheduler.schedule(0 minute, 1 minute, checker, "check")
  }

  def shutdown() = {
    system.shutdown()
  }

}

class ArrivalRandomizer extends Actor {
  val log = Logging(context.system, this)
  def receive = {
    case "status" => log.info("running")
  }
}

class ArrivalListener(aCallback: ArrivalCallback) extends Actor {

  val log = Logging(context.system, this)
  def receive = {
    case a: Arrival => {
      log.info(aCallback.notification.id + " received "+a.route+" at "+DateTime.now.toString("yyyy-dd-mm hh:mm:ss.sss"))
      a.route match {
        case route => {
          if (a.minutesUntilArrival <=aCallback.notification.alarmTime) {
            callback
          }
        }
      }
    }
  }

  def callback = {
     aCallback.callback
  }

  override def postStop = {
    log.info(aCallback.notification.id + " stopped")
  }
}

class ArrivalChecker extends Actor {
  val log = Logging(context.system, this)
  def receive = {
    case n: Notification => {
      log.info("adding "+n)
      context.actorOf(Props(classOf[ArrivalListener], new DefaultCallback(n)))
    }
    case ac: ArrivalCallback => {
      log.info("adding " + ac)
      context.actorOf(Props(classOf[ArrivalListener], ac))
    }
    case "check" => {
      log.info("checking with TriMet")
      context.children.foreach(_ ! Arrival(44, 11))
      log.info(context.children.size + " notified")
    }
    case _ => log.info(sender + " said WAT?")
  }
}

case class Notification(id: String, route: Int, alarmTime: Int)

abstract class ArrivalCallback(val notification: Notification) {
  def callback
}

class DefaultCallback(notification: Notification) extends ArrivalCallback(notification) {
  def callback = {
    println("Notifying user his ride is almost here")
  }
}
case class Arrival(route: Int, minutesUntilArrival: Int)
