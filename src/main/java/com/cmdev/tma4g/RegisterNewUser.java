package com.cmdev.tma4g;

import java.io.IOException;
import java.util.logging.Logger;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.mirror.model.NotificationConfig;
import com.google.api.services.mirror.model.TimelineItem;

import com.google.glassware.AuthUtil;
import com.google.glassware.MirrorClient;

public class RegisterNewUser {
    private static final Logger LOG = Logger.getLogger(RegisterNewUser.class.getSimpleName());

    /**
     * <ul>
     * <li>Subscribe to timeline ?</li>
     * <li>Write user record to storage</li>
     * <li>Send welcome message</li>
     */
				

    public static void registerUser(String userId) throws IOException {
	Credential credential = AuthUtil.newAuthorizationCodeFlow().loadCredential(userId);
	welcomeUser(credential, userId);
    }


    private static void welcomeUser(Credential credential, String userId) throws IOException {

	TimelineItem timelineItem = new TimelineItem();
	timelineItem.setText("Welcome to TriMet Arrivals for Glass");
	timelineItem.setNotification(new NotificationConfig().setLevel("DEFAULT"));
	TimelineItem insertedItem = MirrorClient.insertTimelineItem(credential, timelineItem);
	LOG.info("Bootstrapper inserted welcome message " + insertedItem.getId() + " for user "
        + userId);
    }
						      
}
