package com.cmdev.tma4g;

public class AlertSubscription {
	
	private String userId;
	private String stopId;
	
	public AlertSubscription(String userId, String stopId) {
		this.userId = userId;
		this.stopId = stopId;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStopId() {
		return stopId;
	}
	public void setStopId(String stopId) {
		this.stopId = stopId;
	}

}
