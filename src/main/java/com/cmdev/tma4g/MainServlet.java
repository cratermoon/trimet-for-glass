/*
 * Copyright (C) 2013 Steven E. Newton
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.cmdev.tma4g;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.mirror.model.MenuItem;
import com.google.api.services.mirror.model.NotificationConfig;
import com.google.api.services.mirror.model.TimelineItem;
import com.google.glassware.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cmdev.trimet.Arrival;

/**
 * Handles POST requests from index.jsp
 *
 * @author Steven E. Newton - https://plus.google.com/117085721814217724716
 * @author Jenny Murphy - http://google.com/+JennyMurphy
 */
public class MainServlet extends HttpServlet {

  private static final long serialVersionUID = 6409558523906980365L;

  private static final Logger LOG = Logger.getLogger(MainServlet.class.getSimpleName());
  public static final String CONTACT_ID = "com.cmdev.tma4g.contact.trimet-for-glass";
  public static final String CONTACT_NAME = "TriMet Arrivals for Glass";

  private static Map<String, AlertSubscription> alertSubs = new HashMap<String, AlertSubscription>();

  /**
   * Do stuff when buttons on index.jsp are clicked
   */
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {

    String userId = AuthUtil.getUserId(req);
    Credential credential = AuthUtil.newAuthorizationCodeFlow().loadCredential(userId);
    String message = "";

    if (req.getParameter("operation").equals("insertSubscription")) {

      // subscribe (only works deployed to production)
      try {
        MirrorClient.insertSubscription(credential, WebUtil.buildUrl(req, "/notify"), userId,
            req.getParameter("collection"));
        message = "Application is now subscribed to updates.";
      } catch (GoogleJsonResponseException e) {
        LOG.warning("Could not subscribe " + WebUtil.buildUrl(req, "/notify") + " because "
            + e.getDetails().toPrettyString());
        message = "Failed to subscribe. Check your log for details";
      }

    } else if (req.getParameter("operation").equals("deleteSubscription")) {

      // subscribe (only works deployed to production)
      MirrorClient.deleteSubscription(credential, req.getParameter("subscriptionId"));

      message = "Application has been unsubscribed.";

    } else if (req.getParameter("operation").equals("deleteTimelineItem")) {

      // Delete a timeline item
      LOG.fine("Deleting Timeline Item");
      MirrorClient.deleteTimelineItem(credential, req.getParameter("itemId"));

      message = "Timeline Item has been deleted.";
      
    } else if (req.getParameter("operation").equals("setAlert")) {

    	LOG.fine("Inserting Alert Item");

        if (req.getParameter("stopId") != null) {
        	AlertSubscription newSub = new AlertSubscription(AuthUtil.getUserId(req), req.getParameter("stopId"));
        	TimelineItem timelineItem = makeDeletableTimelineItem();
        	timelineItem.setText("Created alert for stop ID "+req.getParameter("stopId"));
            // Triggers an audible tone when the timeline item is received
	    MirrorClient.insertTimelineItem(credential, timelineItem);
            storeAlertSubscription(newSub);
	    nextArrival(credential, newSub.getStopId());
	    message = "New alert created for "+newSub.getStopId();
	}
    	
    } else if (req.getParameter("operation").equals("listAlerts")) {
	listAlertSubscriptions(AuthUtil.getUserId(req));
    } else {
      String operation = req.getParameter("operation");
      LOG.warning("Unknown operation specified " + operation);
      message = "I don't know how to do that";
    }
    WebUtil.setFlash(req, message);
    res.sendRedirect(WebUtil.buildUrl(req, "/"));
  }

    private AlertSubscription listAlertSubscriptions(String userId) {
	return alertSubs.get(userId);
    }

  private void storeAlertSubscription(AlertSubscription newSub) {
      alertSubs.put(newSub.getUserId(), newSub);
      LOG.info("Stored new alert for "+newSub.getUserId());
  }

    private void nextArrival(Credential credential, String stopId) throws IOException {
	TimelineItem timelineItem = makeDeletableTimelineItem();
	Arrival arrival = new Arrival(stopId);
	long when = arrival.nextArrivalLong();
	String arrivalHtml = "<article>"+
	    "<h1>"+arrival.stopDescription+"</h1>"+
	    "<ul>"+
	    "<li>Next Bus: "+arrival.getRoute()+"</li>"+
	    "<li>Arriving in: "+arrival.nextArrival()+"</li>"+
	    "</ul></article>";
	timelineItem.setText(arrival.stopDescription);
	System.out.println(arrivalHtml);
	timelineItem.setTitle(arrival.stopDescription);
	timelineItem.setHtml(arrivalHtml);
	MirrorClient.insertTimelineItem(credential, timelineItem);
    }

    private TimelineItem makeDeletableTimelineItem() {
	TimelineItem timelineItem = new TimelineItem();
	timelineItem.setNotification(new NotificationConfig().setLevel("DEFAULT"));
	MenuItem menuItem = new MenuItem();
	menuItem.setAction("DELETE");
	List<MenuItem> actions = new ArrayList<MenuItem>();
	actions.add(menuItem);
	timelineItem.setMenuItems(actions);
	return timelineItem;
    }
}
