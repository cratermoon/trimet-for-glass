package com.cmdev.trimet.json;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TriMetArrivalsResponse {
	
  public ArrivalResultSet resultSet;

  public String stopDescription;
  public String route;
  public String queryTime;
	
  @JsonIgnoreProperties(ignoreUnknown=true)
  public static class ArrivalResultSet {
    public ArrayList<Location> location;
    public ArrayList<Arrival> arrival;
  }

  @JsonIgnoreProperties(ignoreUnknown=true)
  public static class Location {
    public String desc;
    public int locid;
    public float lat;
    public float lng;
    public String dir;
  }

  @JsonIgnoreProperties(ignoreUnknown=true)
  public static class Arrival {
    public String status;
    public String estimated;
    public String scheduled;
    public String shortSign;
    public String fullSign;
    public boolean departed;
    public int route;
      public int piece;
      public int dir;
      public int block;
      public int locid;
      public boolean detour;
  }
}
