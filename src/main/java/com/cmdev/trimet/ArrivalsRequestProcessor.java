package com.cmdev.trimet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.http.client.fluent.Request;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.cmdev.trimet.json.TriMetArrivalsResponse;

public class ArrivalsRequestProcessor {
    
    private static final Logger LOG = Logger.getLogger(ArrivalsRequestProcessor.class.getSimpleName());

    private List<Location> arrivalLocations;
    private List<Arrival> arrivals;

    private String stopId;

    public List<Location> getLocations() {
	return arrivalLocations;
    }

    public Location getLocation(int index) {
	return arrivalLocations.get(index);
    }

    public List<Arrival> getArrivals() {
	return arrivals;
    }

    public Arrival getArrival(int index) {
	return arrivals.get(0);
    }

    public void request(String stopId) {
	this.stopId = stopId;
        callTriMet();
    }

    private void callTriMet() {
	try {
	    String response = Request.Get("http://developer.trimet.org/ws/V1/arrivals/locIDs/"+stopId+"/appID/6D42B2F400067A1B9B9028E75/json/true")
		.execute().returnContent().asString();
	    parseJson(response);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    private void parseJson(String response) throws JsonParseException, IOException {
	LOG.fine("parsing "+response);
	ObjectMapper mapper = new ObjectMapper();
	TriMetArrivalsResponse arrivalsResponse = mapper.readValue(response, TriMetArrivalsResponse.class);
	LOG.fine("parse complete: "+arrivalsResponse);
	collectLocations(arrivalsResponse.resultSet.location);
	collectArrivals(arrivalsResponse.resultSet.arrival);
    }
    
    private void collectLocations(List<TriMetArrivalsResponse.Location> locationList) {
	arrivalLocations = new ArrayList<Location>(locationList.size());
	for (TriMetArrivalsResponse.Location l: locationList) {
	    arrivalLocations.add(new Location(l));
	}
    }

    private void collectArrivals(List<TriMetArrivalsResponse.Arrival> arrivalList) {
	arrivals = new ArrayList<Arrival>(arrivalList.size());
	for (TriMetArrivalsResponse.Arrival a : arrivalList) {
	    arrivals.add(new Arrival(a));
	}
    }
}

