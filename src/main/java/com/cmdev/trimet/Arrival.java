package com.cmdev.trimet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.http.client.fluent.Request;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.cmdev.trimet.json.TriMetArrivalsResponse;

public class Arrival {

  private static final Logger LOG = Logger.getLogger(Arrival.class.getSimpleName());
  private static final DateTimeFormatter isoDateTimeFormat = ISODateTimeFormat.dateTime();
  private String stopId;
  private long nextArrivalTime = 0;
  private String status;
  private boolean departed;
    private boolean detour;
    private int locationID;
    private int block;
    private int direction;
    private DateTime scheduled;
    private DateTime estimated;
    private String fullSign;
    private String shortSign;
    private int piece;
    private int route;
   
  private List<TriMetArrivalsResponse.Location> locations;
  private List<Location> parsedLocations;

  public String stopDescription;
  public String nextArrivalTimeStamp;
  public String queryTime;
    
    public int getRoute() {
	return route;
    }

  public String getStatus() {
    return status;
  }

  public boolean isDeparted() {
    return departed;
  }

    public String getFullSignText() {
	return fullSign;
    }

    public boolean isDetoured() {
	return detour;
    }

  public Location getLocation(int index) {
      LOG.info("locations is: "+locations);
      LOG.fine("locations size is "+locations.size());
      Location loc = new Location(locations.get(index));
      if (parsedLocations == null) {
	  parsedLocations = new ArrayList<Location>(locations.size());
      }
      parsedLocations.add(index, loc);
      return loc;
  }

  public Arrival(TriMetArrivalsResponse.Arrival arrival) {
      // map all the json fields to the model object fields
      //"detour":false,"status":"estimated","locid":971,"block":4403,"scheduled":"2014-01-02T10:53:41.000-0800","shortSign":"44 To St Johns","dir":1,"estimated":"2014-01-02T10:53:27.000-0800","route":44,"departed":true,"blockPosition":{"at":"2014-01-02T10:48:37.000-0800","feet":7569,"lng":-122.7221974,"trip":[{"progress":6254,"desc":"Lombard & Pier Park","pattern":2,"dir":1,"route":44,"tripNum":"4219919","destDist":13824}],"lat":45.4519641,"heading":26},"fullSign":"44  Capitol Hwy/Mocks Crest to St Johns via City Center","piece":"1"
      detour = arrival.detour;
      locationID = arrival.locid;
      block = arrival.block;
      route = arrival.route;
      departed = arrival.departed;
      shortSign = arrival.shortSign;
      direction = arrival.dir;
      fullSign = arrival.fullSign;
      piece = arrival.piece;
      status = arrival.status;
      LOG.info("Status: "+status+", Estimated: "+arrival.estimated);
      if (status == "estimated") {
	  estimated = isoDateTimeFormat.parseDateTime(arrival.estimated);
      } else if (status == "scheduled") {
	  scheduled = isoDateTimeFormat.parseDateTime(arrival.scheduled);
      }
  }

  public Arrival(String stopId) {
    this.stopId = stopId;
    callTriMet();
  }
	
  public String nextArrival() {
    return nextArrivalTime == 0 ? "now" : String.valueOf(nextArrivalTime);
  }
	
  public Long nextArrivalLong() {
    return nextArrivalTime;
  }

	private void callTriMet() {
		nextArrivalTime = -1;
		try {
			String response = Request.Get("http://developer.trimet.org/ws/V1/arrivals/locIDs/"+stopId+"/appID/6D42B2F400067A1B9B9028E75/json/true")
				.execute().returnContent().asString();
			nextArrivalTime = parseJson(response);
			if (nextArrivalTime < 0) {
			    nextArrivalTime = 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Long parseJson(String response) throws JsonParseException, IOException {
	    LOG.fine("parsing "+response);
		ObjectMapper mapper = new ObjectMapper();
		TriMetArrivalsResponse arrivals = mapper.readValue(response, TriMetArrivalsResponse.class);
		LOG.fine("parse complete: "+arrivals);
		TriMetArrivalsResponse.Arrival firstArrival = arrivals.resultSet.arrival.get(0);

		nextArrivalTimeStamp = firstArrival.estimated;
		if (nextArrivalTimeStamp == null) { // no estimated time, get scheduled
		    nextArrivalTimeStamp = firstArrival.scheduled;
		}
		locations = arrivals.resultSet.location;
		stopDescription =  arrivals.resultSet.location.get(0).desc;
		shortSign =  firstArrival.shortSign;
		route = firstArrival.route;
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		DateTime arrive = fmt.parseDateTime(nextArrivalTimeStamp);
		Duration dur = new Duration(new DateTime(), arrive);
		return dur.getStandardMinutes();
	}
}
